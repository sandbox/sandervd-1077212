<?php 

class josso_drupal_user {
  //Definition of the class variables -->
  public $name;
  public $data;
  public $sid;

  //--> End of defines
  
  //Contstuctor of the josso user object
  function __construct() {
    $josso_agent = & jossoagent::getNewInstance();
    $ssoSessionId = $josso_agent->accessSession();
    $josso_user = $josso_agent->getUserInSession();
    if(!isset($josso_user))
      //Not logged in...
      return 1;

    //Switch depending on which Jossoversion is selected in the admin interface.
    switch (JOSSO_IDP_VERSION) {
      case 0:      //Version 1.6
      case 1:      //Version 1.8
        $this->name = $josso_user->getName();
        $this->sid = $josso_agent->getSessionId();
        $this->data = $josso_user->getProperties();
        break;
      default:     //Unsupported version
          return;
          break;
    }
  }
  /**
   * Creates a login url for the current page, use to create links to JOSSO login page
   */
  function CreateLoginUrl() {
      // Get JOSSO Agent instance
      $josso_agent = & jossoagent::getNewInstance(); //Nodig?
      $loginUrl = base_path().'josso/login/'. drupal_get_path_alias($_GET['q']);
      return $loginUrl;
  }

  /**
   * Creates a logout url for the current page, use to create links to JOSSO logout page
   */
  function CreateLogoutUrl() {
    // Get JOSSO Agent instance
    $josso_agent = & jossoagent::getNewInstance(); //Nodig?
    $logoutUrl =  base_path().'josso/logout/'. drupal_get_destination();
    return $logoutUrl;
  }

  function RequestLoginForUrl($currentUrl) {
    global $base_root;
    global $base_path;
    $_SESSION['JOSSO_ORIGINAL_URL'] = $currentUrl;
    // Get JOSSO Agent instance
    $josso_agent = & jossoagent::getNewInstance();
    $securityCheckUrl = $base_root . $base_path .'josso/security-check';
    $loginUrl = JOSSO_GATEWAYLOGINURL . '?josso_back_to=' . $securityCheckUrl;
    $loginUrl = $loginUrl . $this->createFrontChannelParams();
    drupal_goto($loginUrl);
    
  }

  function RequestLogoutForUrl($currentUrl) {
    global $base_root;
    global $base_path;
    $_SESSION['JOSSO_ORIGINAL_URL'] = $currentUrl;
      // Get JOSSO Agent instance
      $josso_agent = & jossoagent::getNewInstance();
      //Dit moet beter kunnen, er is waarschijnlijk een Drupal functie voor: get_url?
      $backPath= $base_root . $base_path . $currentUrl;
      $logoutUrl = JOSSO_GATEWAYLOGOUTURL . '?josso_back_to=' . $backPath;
      $logoutUrl = $logoutUrl . $this->createFrontChannelParams();
      // Clear SSO Cookie
      setcookie("JOSSO_SESSIONID", '', 0, "/"); // session cookie ...
      $_COOKIE['JOSSO_SESSIONID'] = '';
      drupal_goto($logoutUrl);
  }

  private function createFrontChannelParams() {
      // Add some request parameters like host name
      $host = $_SERVER['HTTP_HOST'];
      $params = '&josso_partnerapp_host=' . $host;
      return $params;
      // TODO : Support josso_partnerapp_ctx param too ?
  }
  function getByAssertion() {
    $josso_agent = & jossoagent::getNewInstance(); 
    $assertionId = $_REQUEST['josso_assertion_id'];
    return $josso_agent->resolveAuthenticationAssertion($assertionId);
  }
}

