<?php
// $Id$
/**
* @file
* Administration page callbacks for the josso module.
*/

/**
* Form builder. Configure josso.
*
* @ingroup forms
* @see system_settings_form().
*/
function josso_admin_settings() {
$form['josso_idp_version'] = array(
  '#type' => 'select',
  '#title' => t('Version of josso'),
  '#options' => array(t('Version 1.6'), t('Version 1.8')),
  '#default_value' => variable_get('josso_idp_version', ''),
  '#description' => t('Josso software version.'),
);
$form['josso_gatewayLoginUrl'] = array(
  '#type' => 'textfield',
  '#title' => t('Gateway login Url'),
  '#default_value' => variable_get('josso_gatewayLoginUrl', 'http://localhost:8080/josso/signon/login.do'),
  '#description' => t('The Gateway Login URL, which represents the URL where the user should be redirected to on protected resource access, so that he has a chance to authenticate itself.'),
);
$form['josso_gatewayLogoutUrl'] = array(
  '#type' => 'textfield',
  '#title' => t('Gateway logout Url'),
  '#default_value' => variable_get('josso_gatewayLogoutUrl', 'http://localhost:8080/josso/signon/logout.do'),
  '#description' => t('The Gateway Logout URL, which represents the URL where the user should be redirected on logout request.'),
);
$form['josso_endpoint'] = array(
  '#type' => 'textfield',
  '#title' => t('Gateway endpoint'),
  '#default_value' => variable_get('josso_endpoint', 'http://localhost:8080'),
  '#description' => t('The Gateway endpoint, which represents where the JOSSO Web Services are listening.'),
);
$form['proxy'] = array(
'#title' => t('Proxy configuration'),
'#type' => 'fieldset',
'#description' => t('Leave blank if unused.'),
'#collapsible' => TRUE,
'#collapsed' => TRUE
);
$form['proxy']['proxyhost'] = array(
  '#type' => 'textfield',
  '#title' => t('Proxy hostname'),
  '#default_value' => variable_get('josso_proxyhost', '')
);
$form['proxy']['proxyport'] = array(
  '#type' => 'textfield',
  '#title' => t('Proxy portnumber'),
  '#default_value' => variable_get('josso_proxyport', '')
);
$form['proxy']['proxyusername'] = array(
  '#type' => 'textfield',
  '#title' => t('Proxy username'),
  '#default_value' => variable_get('josso_proxyusername', '')
);
$form['proxy']['proxypassoword'] = array(
  '#type' => 'textfield',
  '#title' => t('Proxy password'),
  '#default_value' => variable_get('josso_proxypassoword', '')
);
//Still need to make basePath obsolete
return system_settings_form($form);
}